import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        match: {
            teamHome: {
                name: 'Peräkylän perse',
                id: 'pperse',
                players: [

                ],
            },
            teamAway: {
                name: 'Erämaan jormat',
                id: 'ejormat',
                players: [

                ],
            },
            status: 'live',
            properties: {
                'serie': 'something',
                'date': '1.1.2020',
                'time': '20:20',
                'location' : 'Erämaa',
                'timer': '0:00',
            },
            events: [
                {
                    type: 'goal',
                    time: '15:25',
                    props: {
                        team: 'pperse',
                        scorer: 'playerId',
                        assists: [
                            'playerId2',
                            'playerId3'
                        ]
                    }
                },
                {
                    type: 'penalty', 
                    time: '14:53',
                    props: {
                        team: 'ejormat',
                        player: 'playerId',
                        reason: 'penaltyCode',
                    }
                }
            ]
        }
    },
    getters: {
        match: state => {
            return state.match
        },
        matchStatus: state => {
            return state.match.status
        },
        matchEvents: state => {
            return state.match.events;
        },
        goalCount: state => (teamId: string) => {
            return state.match.events.filter(event => (teamId === event.props.team && event.type === 'goal')).length;
        },
    },
    mutations: {
        addEvent(state, event) {
            state.match.events.push(event)
        }
    }
})