export interface Event {
    type: string;
    time: string;
    props: Object;
}